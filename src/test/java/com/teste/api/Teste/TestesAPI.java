package com.teste.api.Teste;

import static com.jayway.restassured.RestAssured.baseURI;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class TestesAPI {

	public TestesAPI(){
		baseURI = "https://jsonplaceholder.typicode.com/todos";
	}

	@Test
	/*Chama o serviço pelo metodo GET*/
	public void testConsultaPorId() {

		 given()
		 .when()
		    .get("/1")
		 .then()
		    .statusCode(200)
		    .body("userId", is(1))
		    .body("id", is(1))
		    .body("title", equalTo("delectus aut autem"))
		    .body("completed", equalTo("false"));
	}

}
